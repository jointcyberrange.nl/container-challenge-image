# Container Challenge Image

This repository houses the Docker image in the Gitlab container registry used to deploy CTFd container challenges in Kubernetes. The image is used by the [container_challenges plugin](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/tree/master/CTFd/plugins/container_challenges) and **must** be publicly available. For more detailed information about this image, read the [documentation](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/blob/master/CTFd/plugins/container_challenges/readme/CONFIGURATION.md). This repository is public because of technical reasons, but the container challenges are not. These challenges are stored in a private [Gitlab group](https://gitlab.com/hu-hc/jcr/challenges) and are only accessable with a [deploy token](https://gitlab.com/hu-hc/jcr/platform/ctf-platform/-/blob/master/CTFd/plugins/container_challenges/readme/CONFIGURATION_UPLOAD.md).

A container is automatically build. 

[![pipeline status](https://gitlab.com/jointcyberrange.nl/container-challenge-image/badges/main/pipeline.svg)](https://gitlab.com/jointcyberrange.nl/container-challenge-image/-/commits/main)

## 1. Used Technologies

- [Ubuntu](https://en.wikipedia.org/wiki/Ubuntu)
- [cURL](https://en.wikipedia.org/wiki/CURL)
- [Kompose](https://kompose.io/)
- [Helm](https://helm.sh/)
- [Base64](https://en.wikipedia.org/wiki/Base64)

## 1. `Dockerfile`

```dockerfile
FROM ubuntu:focal

COPY entrypoint.sh ./entrypoint.sh

RUN chmod +x entrypoint.sh
RUN apt update  \
        && apt install -y curl
RUN curl -fsSL https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 -o get_helm.sh
RUN curl -L https://github.com/kubernetes/kompose/releases/download/v1.22.0/kompose-linux-amd64 -o kompose
RUN chmod +x kompose
RUN chmod +x get_helm.sh
RUN mv ./kompose /usr/local/bin/kompose
RUN ./get_helm.sh

ENTRYPOINT ["./entrypoint.sh"]
```

The Dockerfile of the image installs all the necessary dependencies (Ubuntu, cURL, Kubernetes, Kompose, Helm) needed for the deploying container challenges in Kubernetes. The file/directory rights are also setup. When everything is finished, `entrypoint.sh` is executed.

## 2. `entrypoint.sh`

```sh
#!/bin/bash

## Variables
[ -z "$FILE" ] && echo $timestamp "$FILE is not defined." || echo $timestamp "$FILE is defined as file."
[ -z "$ENCODED" ] && echo $timestamp "$ENCODED is not defined." || echo $timestamp "$ENCODED is defined."
timestamp=$(date +%s)

## Convert base64 encoded compose to file
#Commands
echo $ENCODED | base64 --decode > $FILE

## Generate HELM Chart
# Checks
[ -f $FILE ] && echo $timestamp "$FILE exists." || echo $timestamp "$FILE is non existant."
if ! command -v kompose &> /dev/null
then
    echo "The kompose binary could not be found"
    exit
fi

# Commands
kompose convert -f $FILE -c

## Deploy generated HELM Chart
# Checks
if ! command -v helm &> /dev/null
then
    echo "The Helm.sh binary could not be found"
    exit
fi

# Commands
NAME=`echo "$FILE" | cut -d'.' -f1`
helm upgrade --install $NAME $NAME --wait
helm status $NAME -o json > deploy.json
```

1. The first thing to note is the use of 2 ENV variables:
    * `$FILE`: the name of the container challenge docker-compose file.
    * `$ENCODED`: the base64 encoded contents of the container challenge docker-compose file.
2. Then cURL is validated and used to store the contents in the `$FILE`.
3. Then Kompose is validated and used to convert the docker-compose file into a Helm chart.
4. Then Helm is validated and used to deploy the generated Helm chart into Kubernetes.
5. The container challenge is now running inside of a Kubernetes container.
